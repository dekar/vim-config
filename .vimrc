set nocompatible              " be iMproved, required
filetype off                  " required

if has('win32') || has('win64')
    set rtp+=~/vimfiles/bundle/vundle
    call vundle#rc('~/vimfiles/bundle/vundle')
else
    set rtp+=~/.vim/bundle/vundle
    call vundle#rc()
endif

call vundle#begin()

Plugin 'gmarik/Vundle.vim'
Plugin 'ack.vim'
Plugin 'kien/ctrlp.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'scrooloose/nerdcommenter'
Plugin 'godlygeek/tabular'
Plugin 'majutsushi/tagbar'
Plugin 'scrooloose/nerdtree'
Plugin 'tpope/vim-surround'
Plugin 'Shougo/neocomplete.vim'
Plugin 'Shougo/neosnippet.vim'
Plugin 'Shougo/neosnippet-snippets'
Plugin 'a.vim'
Plugin 'bufexplorer.zip'
"Plugin 'MattesGroeger/vim-bookmarks'
Plugin 'DoxygenToolkit.vim'
Plugin 'xolox/vim-lua-ftplugin'
Plugin 'xolox/vim-misc'
"" plugin on GitHub repo
"Plugin 'tpope/vim-fugitive'
"" plugin from http://vim-scripts.org/vim/scripts.html
"Plugin 'L9'
"" Git plugin not hosted on GitHub
"Plugin 'git://git.wincent.com/command-t.git'
"" git repos on your local machine (i.e. when working on your own plugin)
"Plugin 'file:///home/gmarik/path/to/plugin'
"" The sparkup vim script is in a subdirectory of this repo called vim.
"" Pass the path to set the runtimepath properly.
"Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
"" Avoid a name conflict with L9
"Plugin 'user/L9', {'name': 'newL9'}


" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

colorscheme molokai

"регистро-независимый поиск
"set ic                                  

"умный учет регистра
set smartcase                           

"разрыв строки
set nowrap                              

"размер таба
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab

syntax enable

"???
set showcmd

"высота коммандной строки
set cmdheight=1                         

"инкреметный поиск
set is                                  

"указатели конца строки
set listchars+=precedes:<,extends:>     

"авто сохранение при компиляции
set autowrite                           

"включаем в обработку .vimrc из локального каталога, где запущен редактор.
"удобно для подключения файлов тегов
set exrc                                

"настройка backspace
set backspace=indent,eol,start          

"отключаем бэкапы
"set nobackup                            

"отключаем vi привычки
set nocompatible                        

filetype plugin indent on

nnoremap <up> gk
nnoremap <down> gj
nnoremap j gj
nnoremap k gk

let g:neocomplcache_enable_at_startup = 1
"}}}

"улучшаем прокрутку чтобы курсор возвращался на место при PageUp/PageDown
nmap <PageUp> <C-U><C-U>
imap <PageUp> <C-O><C-U><C-O><C-U>
nmap <PageDown> <C-D><C-D>
imap <PageDown> <C-O><C-D><C-O><C-D>

" Find definition of current symbol using Gtags
map <C-?> <esc>:Gtags -r <CR>
" Find references to current symbol using Gtags
map <C-F> <esc>:Gtags <CR>

"привычный C-del, удаляет слово вперед
imap <C-Del> <ESC>ldwi

"{{{ Русификация
"для набора русских символов в лаинской раскладке
set keymap=russian-jcukenwin 
"а поиск оставляем по английски
set imsearch=0
set iminsert=0

"для корректного перехода по русским словам
set iskeyword=@,48-57,_,192-255         

"можно переключать раскладку в нормальном режиме а не только в режиме вставки
noremap <C-^> a<C-^><ESC>

"при русской раскладке в режиме вставки цвет курсора - красный
hi lCursor guifg=NONE guibg=Red

"{{{ Языковые установки
"настройка для Python
let python_highlight_all = 1

"{{{ Кодировка
"добавляем меню, управляемое табом для выбора кодировки
menu Encoding.cp866         :e ++enc=cp866<CR>
menu Encoding.windows-1251  :e ++enc=cp1251<CR>
menu Encoding.koi8-r        :e ++enc=koi8-r<CR>
menu Encoding.utf-8         :e ++enc=utf8<CR>
map <C-F1> :emenu Encoding.<TAB>
"}}}

"{{{ Складки
"добавляем меню, управляемое табом для выбора режима фолдинга
menu Fold.syntax         :set foldmethod=syntax<CR>
menu Fold.marker         :set foldmethod=marker<CR>
menu Fold.manual         :set foldmethod=manual<CR>
menu Fold.indent         :set foldmethod=indent<CR>
map <C-F3> :emenu Fold.<TAB>

set foldmethod=syntax
set foldlevel=5
"}}}

"{{{ Плагины
"NERD tree
nnoremap <silent> <M-F8> :NERDTreeToggle<CR>
let NERDTreeWinSize=30
let NERDTreeIgnore=['.*\.o', '.*\.a', '.*\.so', 'tags']

"список буферов
map <C-\> :BufExplorer<CR>

"настройка DoxygenToolkit
let g:DoxygenToolkit_compactDoc = "yes"
let g:DoxygenToolkit_briefTag_pre = ""
let g:DoxygenToolkit_paramTag_pre = "@param  "
let g:DoxygenToolkit_returnTag =    "@return "

"включает автоматическую doxygen подсветку
let g:load_doxygen_syntax=1 
"}}}

"{{{ Разработка
"компиляция  
map <F9> :make<CR>
"устанавливаем scons вместо make
"set makeprg=scons
"set makeprg=mingw32-make
au BufRead,BufNewFile *.proto set filetype=proto
au BufRead,BufNewFile *.json set filetype=javascript
au BufRead,BufNewFile SConstruct set filetype=python
au BufRead,BufNewFile SConscript set filetype=python

"создание тэгов в текущем каталоге
map <C-F9> :!ctags -R --c++-kinds=+p --exclude=Session.vim --exclude=\*.html --fields=+iaS --extra=+q .<CR>
"}}}

"{{{ Разное
"сохранение файла
map <F2> :w<CR>
"сохранение всех файлов
map <C-F2> :wall<CR>

"перемещение по ошибкам
map <C-S-Right> :clast<CR><ESC>
map <C-S-Left> :cfirst<CR><ESC>
map <C-S-Up> :cprevious<CR><ESC>
map <C-S-Down> :cnext<CR><ESC>

map <C-S-l> :clast<CR><ESC>
map <C-S-h> :cfirst<CR><ESC>
map <C-S-k> :cprevious<CR><ESC>
map <C-S-j> :cnext<CR><ESC>

"переключение в меню табом
set wildcharm=<TAB>

"перезагрузка .vimrc при его сохранении
autocmd bufwritepost $MYVIMRC source $MYVIMRC

"подсветка строчек длинне 80 символов
set colorcolumn=80

"добавляем Doxygen комментарий
autocmd Filetype c,cpp set comments^=:///

"}}}

"{{{ Markdown
augroup mkd
autocmd BufRead *.mkd  set ai formatoptions=tcroqn2 comments=n:>
augroup END
"}}}

au BufNewFile,BufRead *.i set filetype=swig 
au BufNewFile,BufRead *.swg set filetype=swig

noremap <F12> :set list!<CR>
noremap <F10> :set relativenumber!<CR>
nnoremap <silent> <F8> :Tagbar<CR>

if has('win32') 
    set encoding=utf-8
    setglobal fileencoding=utf-8 bomb
    set fileencodings=utf-8,cp1251,koi8-r,latin1
    set fileformats=dos,unix,mac

    "добавляем общий тег-файл
    "set tags+=$HOME/.vim/systags/tags
    "set tags+=/MinGW/include/tags
else
    "добавляем общий тег-файл
    set tags+=$HOME/tags
endif

set autoread
set foldopen-=block

map <leader>b :FufBuffer<CR>
map <leader>l :FufLine<CR>

map <leader>r :w<CR>:!C:\Python27\python.exe % <CR>

g:neocomplete#enable_at_startup = 1

let g:ctrlp_custom_ignore = {
            \ 'dir':  '\v[\/]\.(git|hg|svn)$',
            \ 'file': '\v\.(exe|so|dll|o|a)$',
            \ }
let g:ctrlp_max_depth = 4

"Impontantable Killer feature
autocmd InsertLeave * if pumvisible() == 0|pclose|endif"

set noeb vb t_vb=

let g:bookmark_manage_per_buffer = 1

" Plugin key-mappings.
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)
xmap <C-k>     <Plug>(neosnippet_expand_target)

" SuperTab like snippets behavior.
imap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)"
\: pumvisible() ? "\<C-n>" : "\<TAB>"
smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)"
\: "\<TAB>"

" For conceal markers.
if has('conceal')
  set conceallevel=2 concealcursor=niv
endif
