if has('win32') || has('win64')
    set guifont=Consolas:h12 "шрифт
endif

set columns=102 
set lines=58                            "размеры окна

set guioptions-=m                      "скрываем меню
set guioptions-=T                      "панель инструментов
set guioptions-=L                      "полосу прокрутки слева

map <C-F7> :set guioptions-=m<CR>
map <C-F8> :set guioptions+=m<CR>

map <F11> <Esc>:call libcallnr("gvimfullscreen.dll", "ToggleFullScreen", 0)<CR>
